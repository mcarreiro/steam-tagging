package models;

import java.util.Date;

import org.joda.time.DateTime;
import org.mongodb.morphia.annotations.Entity;

import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import dtos.GameDto;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.modules.morphia.Model;
import utils.JsonUtils;

@Entity
public class Game extends Model {

	public String gameId;
	public String gameName;
	public float price;
	public String description;
	public String image;
	public String background;
	public boolean singleplayer;
	public boolean multiplayer;
	public boolean coop;
	public boolean coopLocal;
	public boolean forWin;
	public boolean forMac;
	public boolean forLinux;
	public Date lastUpdated;

	public static Game createGame(GameDto dto) {
		Game game = new Game();
		game.gameId = dto.id;
		game.gameName = dto.name;
		// game.price = price;
		game.description = dto.description;
		game.image = dto.image;
		game.background = dto.background;
		game.singleplayer = dto.singleplayer;
		game.multiplayer = dto.multiplayer;
		game.coop = dto.coop;
		game.coopLocal = dto.coopLocal;
		game.forWin = dto.forWin;
		game.forMac = dto.forMac;
		game.forLinux = dto.forLinux;
		game.save();
		return game;
	}

	public static Game createGame(String gameId, String gameName, float price, String description, String image,
			String background, boolean singleplayer, boolean multiplayer, boolean coop, boolean coopLocal,
			boolean forWin, boolean forMac, boolean forLinux) {

		Game game = new Game();
		game.gameId = gameId;
		game.gameName = gameName;
		game.price = price;
		game.description = description;
		game.image = image;
		game.background = background;
		game.singleplayer = singleplayer;
		game.multiplayer = multiplayer;
		game.coop = coop;
		game.coopLocal = coopLocal;
		game.forWin = forWin;
		game.forMac = forMac;
		game.forLinux = forLinux;
		game.save();
		return game;
	}

	public static Game findByGameId(String appId) {
		MorphiaQuery q = Game.q();
		q.field("gameId").equal(appId);
		return q.first();
	}

	public static GameDto toDto(Game game) {
		return GameDto.createGame(game.gameId, game.gameName, game.price, game.description, game.image, game.background,
				game.singleplayer, game.multiplayer, game.coop, game.coopLocal, game.forWin, game.forMac,
				game.forLinux);
	}

	public String getGameName(String gameId) {
		if (!Strings.isNullOrEmpty(this.gameName)) {
			return this.gameName;
		} else {
			HttpResponse r = WS.url("http://store.steampowered.com/api/appdetails/?appids=" + gameId).get();
			JsonElement e = r.getJson().getAsJsonObject();
			e = JsonUtils.getJsonObjectFromJson(e.getAsJsonObject(), gameId);
			e = JsonUtils.getJsonObjectFromJson(e.getAsJsonObject(), "data");
			String name = JsonUtils.getStringFromJson(e.getAsJsonObject(), "name");
			return name;
		}
	}

	public static Game createGame(String gameId) {
		HttpResponse respo = WS.url("http://store.steampowered.com/api/appdetails/?appids=" + gameId).get();
		return createGame(respo);
	}

	public static GameDto createGameDto(HttpResponse respo) {
		GameDto gameDto = new GameDto();

		JsonObject o, o1, o2, data, platformData;
		JsonArray categories;
		String a, success = "";

		JsonElement jsonResp = respo.getJson();
		a = jsonResp.toString();
		String appId = a.substring(2, a.indexOf("\":{"));
		o = jsonResp.getAsJsonObject();
		o1 = JsonUtils.getJsonObjectFromJson(o, appId + "").getAsJsonObject();
		success = JsonUtils.getStringFromJson(o1, "success");
		gameDto.id = appId;
		if ("true".equals(success)) {
			data = JsonUtils.getJsonObjectFromJson(o1, "data").getAsJsonObject();
			gameDto.name = JsonUtils.getStringFromJson(data, "name");
			gameDto.image = JsonUtils.getStringFromJson(data, "header_image");
			gameDto.background = JsonUtils.getStringFromJson(data, "background");

			platformData = JsonUtils.getJsonObjectFromJson(data, "platforms").getAsJsonObject();

			gameDto.forWin = JsonUtils.getBooleanFromJson(platformData, "windows");
			gameDto.forMac = JsonUtils.getBooleanFromJson(platformData, "mac");
			gameDto.forLinux = JsonUtils.getBooleanFromJson(platformData, "linux");

			categories = JsonUtils.getJsonArrayFromJson(data, "categories");
			if (categories != null) {
				for (int j = 0; j < categories.size(); j++) {
					o2 = categories.get(j).getAsJsonObject();

					String description = JsonUtils.getStringFromJson(o2, "description");
					gameDto.description = description;
					if (description.equalsIgnoreCase("Multi-player") || description.equalsIgnoreCase("Multiplayer")) {
						gameDto.multiplayer = true;
					}
					if (description.equalsIgnoreCase("Single-player") || description.equalsIgnoreCase("Singleplayer")) {
						gameDto.singleplayer = true;
					}
					if (description.contains("Coop") || description.contains("Co-op")) {
						gameDto.coop = true;
					}
					if ((description.contains("Co-op") || description.contains("Coop"))
							&& (description.contains("local") || description.contains("Local"))) {
						gameDto.coopLocal = true;
					}
				}
			}
			Game.createGame(gameDto);
		}

		return gameDto;

	}

	public static Game createGame(HttpResponse respo) {
		GameDto gameDto = new GameDto();
		Game game = null;
		JsonObject o, o1, o2, data, platformData;
		JsonArray categories;
		String a, success = "";

		JsonElement jsonResp = respo.getJson();
		a = jsonResp.toString();
		String appId = a.substring(2, a.indexOf("\":{"));
		o = jsonResp.getAsJsonObject();
		o1 = JsonUtils.getJsonObjectFromJson(o, appId + "").getAsJsonObject();
		success = JsonUtils.getStringFromJson(o1, "success");
		gameDto.id = appId;
		if ("true".equals(success)) {
			data = JsonUtils.getJsonObjectFromJson(o1, "data").getAsJsonObject();
			gameDto.name = JsonUtils.getStringFromJson(data, "name");
			gameDto.image = JsonUtils.getStringFromJson(data, "header_image");
			gameDto.background = JsonUtils.getStringFromJson(data, "background");

			platformData = JsonUtils.getJsonObjectFromJson(data, "platforms").getAsJsonObject();

			gameDto.forWin = JsonUtils.getBooleanFromJson(platformData, "windows");
			gameDto.forMac = JsonUtils.getBooleanFromJson(platformData, "mac");
			gameDto.forLinux = JsonUtils.getBooleanFromJson(platformData, "linux");

			categories = JsonUtils.getJsonArrayFromJson(data, "categories");
			if (categories != null) {
				for (int j = 0; j < categories.size(); j++) {
					o2 = categories.get(j).getAsJsonObject();

					String description = JsonUtils.getStringFromJson(o2, "description");
					gameDto.description = description;
					if (description.equalsIgnoreCase("Multi-player") || description.equalsIgnoreCase("Multiplayer")) {
						gameDto.multiplayer = true;
					}
					if (description.equalsIgnoreCase("Single-player") || description.equalsIgnoreCase("Singleplayer")) {
						gameDto.singleplayer = true;
					}
					if (description.contains("Coop") || description.contains("Co-op")) {
						gameDto.coop = true;
					}
					if ((description.contains("Co-op") || description.contains("Coop"))
							&& (description.contains("local") || description.contains("Local"))) {
						gameDto.coopLocal = true;
					}
				}
			}
			game = Game.createGame(gameDto);
		}

		return game;

	}

}
