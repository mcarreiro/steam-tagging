package models;

import java.util.List;

import org.mongodb.morphia.annotations.Entity;

import play.modules.morphia.Model;

@Entity
public class UserDiscount extends Model {

	public String email;

	public static UserDiscount createUserDiscount(String email, boolean save) {
		UserDiscount user = new UserDiscount();
		user.email = email;
		if (save) {
			user.save();
		}
		return user;
	}

	public static UserDiscount findByEmail(String userEmail) {
		MorphiaQuery q = UserDiscount.q();
		q.field("email").equal(userEmail);
		return q.first();
	}

	public List<GameDiscount> getGameList() {
		MorphiaQuery q = GameDiscount.q();
		q.field("user").equal(this);
		return q.fetchAll();
	}

}
