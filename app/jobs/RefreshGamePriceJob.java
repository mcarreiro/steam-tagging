package jobs;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

import com.google.gson.JsonObject;

import models.Game;
import play.jobs.Every;
import play.jobs.Job;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.modules.morphia.Model.MorphiaQuery;
import utils.JsonUtils;

@Every("30s")
public class RefreshGamePriceJob extends Job {

	@Override
	public void doJob() {
		System.out.println("Refreshing game prices");
		MorphiaQuery q = Game.q();
		q.or(q.criteria("price").equal(999999), q.criteria("lastUpdated").doesNotExist(),
				q.criteria("lastUpdated").lessThan(DateTime.now().minusHours(6).toDate()));
		List<Game> gameList = q.fetchAll();
		for (Game game : gameList) {
			game.price = getGamePrice(game.gameId);
			game.lastUpdated = new Date();
			game.save();
		}
		System.out.println("job ended");
	}

	private float getGamePrice(String gameId) {
		float finalPrice = 999999f;
		try {
			HttpResponse respo = WS.url("http://store.steampowered.com/api/appdetails/?appids=" + gameId).get();
			JsonObject o = respo.getJson().getAsJsonObject();
			JsonObject o1 = JsonUtils.getJsonObjectFromJson(o, gameId).getAsJsonObject();
			JsonObject data = JsonUtils.getJsonObjectFromJson(o1, "data").getAsJsonObject();
			JsonObject prices = JsonUtils.getJsonObjectFromJson(data, "price_overview").getAsJsonObject();
			String price = "999999";
			if (prices != null) {
				price = JsonUtils.getStringFromJson(prices, "final");
				if (price.length() == 4) {
					price = price.substring(0, 2) + "." + price.substring(2, 4);
				} else if (price.length() == 3) {
					price = price.substring(0, 1) + "." + price.substring(1, 3);
				}
			}
			finalPrice = Float.parseFloat(price);

			// Ver el else de este caso que juegos no tienen precio.
		} catch (Exception e) {
			System.out
					.println("Error obteniendo precio: http://store.steampowered.com/api/appdetails/?appids=" + gameId);
//			e.printStackTrace();
		}

		return finalPrice;
	}

}
