package dtos;

import models.Game;

public class PlayerDto {

	public String id;
	public String nickName;
	public String realName;
	public String profileUrl;
	public String avatar;
	public String avatarMedium;
	public String avatarFull;

}