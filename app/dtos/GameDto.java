package dtos;

import models.Game;

public class GameDto {

	public String id;
	public String name;
	public String description;
	public String image;
	public String background;
	public boolean singleplayer;
	public boolean multiplayer;
	public boolean coop;
	public boolean coopLocal;
	public boolean forWin;
	public boolean forMac;
	public boolean forLinux;

	public static GameDto createGame(String gameId, String gameName, float price, String description, String image,
			String background, boolean singleplayer, boolean multiplayer, boolean coop, boolean coopLocal,
			boolean forWin, boolean forMac, boolean forLinux) {

		GameDto game = new GameDto();
		game.id = gameId;
		game.name = gameName;
		game.description = description;
		game.image = image;
		game.background = background;
		game.singleplayer = singleplayer;
		game.multiplayer = multiplayer;
		game.coop = coop;
		game.coopLocal = coopLocal;
		game.forWin = forWin;
		game.forMac = forMac;
		game.forLinux = forLinux;
		
		return game;
	}

	public String getGameName() {
		String result = this.name;
		if (this.singleplayer) {
			if (this.name.length() > 22) {
				result = this.name.substring(0, 22) + "...";
			} else if (this.name.length() > 31) {
				result = this.name.substring(0, 28) + "...";
			}
		}
		return result;
	}

}